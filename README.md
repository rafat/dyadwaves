A Simple Graphical User Interface for Discrete Wavelet Transform build using C++/QT4. 

Documentation - http://dyadwaves.sourceforge.net/

Binaries - http://sourceforge.net/projects/dyadwaves/files/ 

Binaries 2 -  https://bitbucket.org/rafat/dyadwaves/downloads

Contact rafat.hsn@gmail.com